package main

import (
	"github.com/dgrijalva/jwt-go"
	"log"
	"os"
	users "saFinal/user/domain"
	"time"
)

const tokenLifeTime = 20

type MyCustomClaims struct {
	UserId   int64  `json:"userId"`
	Username string `json:"username"`
	Role     string `json:"role"`
	jwt.StandardClaims
}

func GenerateToken(user *users.User) (string, error) {
	var err error
	secret := os.Getenv("SECRET")

	claims := MyCustomClaims{
		UserId:   user.Id,
		Username: user.Username,
		Role:     user.Role,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * tokenLifeTime).Unix(),
			Issuer:    "aitu",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	/*token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"iss":      "aitu",
		"userId":   user.Id,
		"username": user.Username,
		"role":     user.Role,
		"exp":      time.Now().Add(time.Minute * tokenLifeTime).Unix(),
	})*/

	tokenString, err := token.SignedString([]byte(secret))

	if err != nil {
		log.Fatal(err)
	}

	return tokenString, nil
}
