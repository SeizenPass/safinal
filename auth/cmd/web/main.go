package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"os"
	"saFinal/user/repository"
	"saFinal/user/services"
)

type application struct {
	errorLog *log.Logger
	infoLog  *log.Logger
	router   *gin.Engine
	service  AuthServiceInterface
}

func main() {

	username := flag.String("username", "postgres", "username")
	password := flag.String("password", "postgres", "password")

	host := flag.String("host", "localhost", "host")
	port := flag.String("port", "5432", "port")
	dbname := flag.String("dbname", "anime", "dbname")
	addr := flag.String("addr", ":4000", "HTTP network address")

	secret := flag.String("secret", "NdRgUkXp2s5v8y/B?E(G+KbPeShVmYq3", "Secret key")

	flag.Parse()

	err := os.Setenv("SECRET", *secret)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to set secret key: %v\n", err)
		os.Exit(1)
	}

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	connString := fmt.Sprintf("postgres://%v:%v@%v:%v/%v", *username, *password, *host, *port, *dbname)

	conn, err := pgxpool.Connect(context.Background(), connString)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}

	defer conn.Close()

	userRepository := &repository.UserRepository{
		Pool:     conn,
		ErrorLog: errorLog,
		InfoLog:  infoLog,
	}

	usersService := &services.UsersService{Repository: userRepository}

	router := gin.Default()

	app := &application{
		router:   router,
		errorLog: errorLog,
		infoLog:  infoLog,
		service:  usersService,
	}
	app.routes()

	infoLog.Printf("Starting server on %s", *addr)
	err = router.Run(*addr)
	if err != nil {
		app.errorLog.Fatal(err)
	}

}
