package main

import "saFinal/utils/token"

func (a *application) routes() {
	a.router.POST("/signup", a.SignUp)
	a.router.POST("/login", a.Login)
	a.router.GET("/protectedEndpoint", token.VerifyMiddleware(), a.Protected)
}
