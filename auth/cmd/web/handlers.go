package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"saFinal/foundation/rest_errors"
	users "saFinal/user/domain"
)

type AuthServiceInterface interface {
	//GetUser(int64) (*users.User, rest_errors.RestErr)
	CreateUser(*users.User, bool) (*users.User, rest_errors.RestErr)
	/*UpdateUser(bool, *users.User) (*users.User, rest_errors.RestErr)
	DeleteUser(int64) rest_errors.RestErr
	SearchUser(string) (users.Users, rest_errors.RestErr)*/
	LoginUser(*users.LoginRequest) (*users.User, rest_errors.RestErr)
}

type DummyResponse struct {
	DummyResponse string `json:"dummyResponse"`
}

func (a *application) Login(c *gin.Context) {
	loginRequest := &users.LoginRequest{}
	if err := c.ShouldBindJSON(loginRequest); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}

	/*if err := user.Validate(); err != nil {
		c.JSON(err.Status(), err)
		return
	}*/

	user, err := a.service.LoginUser(loginRequest)

	if err != nil {
		c.JSON(err.Status(), err)
		return
	}

	tokenObj, genErr := GenerateToken(user)

	if genErr != nil {
		genRestError := rest_errors.NewInternalServerError("error during token generation", genErr)
		c.JSON(genRestError.Status(), genRestError)
		return
	}
	c.JSON(http.StatusOK, tokenObj)
}

func (a *application) SignUp(c *gin.Context) {
	user := &users.User{}
	if err := c.ShouldBindJSON(user); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}

	result, saveErr := a.service.CreateUser(user, false)

	if saveErr != nil {
		c.JSON(saveErr.Status(), saveErr)
		return
	}
	//c.JSON(http.StatusCreated, result.Marshall(c.GetHeader("X-Public") == "true"))
	c.JSON(http.StatusCreated, result.Marshall())
}

func (a *application) Protected(c *gin.Context) {
	dm := &DummyResponse{DummyResponse: "dummy response was reached"}
	c.JSON(http.StatusOK, dm)
}
