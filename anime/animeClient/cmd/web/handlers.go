package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	anime "saFinal/anime/animeServer/domain"
	"saFinal/foundation/rest_errors"
	"saFinal/utils/token"
	"strconv"
)

type AnimeClientServiceInterface interface {
	GetAnime(animeId int) (*anime.Anime, rest_errors.RestErr)
	AddAnime(animeObj *anime.Anime) (int32, rest_errors.RestErr)
	GetAllAnime() ([]*anime.Anime, rest_errors.RestErr)
	SearchAnime(keyword string) ([]*anime.Anime, rest_errors.RestErr)
	DeleteAnime(animeId int) (int32, rest_errors.RestErr)
	AddToFavourite(userId int, animeId int) (bool, rest_errors.RestErr)
	IsFavourite(userId int, animeId int) (bool, rest_errors.RestErr)
	GetAllFavouriteAnime(userId int) ([]*anime.Anime, rest_errors.RestErr)
}

func (a *application) GetAnime(c *gin.Context) {
	animeId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}

	response, errGet := a.service.GetAnime(animeId)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}

	c.JSON(http.StatusOK, response)
	//c.JSON(http.StatusOK, userResponse.Marshall())
}

func (a *application) AddAnime(c *gin.Context) {
	animeObj := &anime.Anime{}
	if err := c.ShouldBindJSON(&animeObj); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}

	response, errGet := a.service.AddAnime(animeObj)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}

	c.JSON(http.StatusCreated, response)
}

func (a *application) GetAllAnime(c *gin.Context) {
	response, errGet := a.service.GetAllAnime()
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}
	c.JSON(http.StatusOK, response)
}

func (a *application) SearchAnime(c *gin.Context) {
	keyword := c.Param("keyword")
	if keyword == "" {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}
	response, errGet := a.service.SearchAnime(keyword)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}
	c.JSON(http.StatusOK, response)
}

func (a *application) DeleteAnime(c *gin.Context) {
	animeId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}

	val, errGet := a.service.DeleteAnime(animeId)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}

	c.JSON(http.StatusOK, val)
}

func (a *application) AddToFavourite(c *gin.Context) {
	animeId, err := strconv.Atoi(c.Param("anime"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}
	userId := token.GetIdFromJWT(c)
	val, errGet := a.service.AddToFavourite(userId, animeId)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}

	c.JSON(http.StatusOK, val)
}

func (a *application) IsFavourite(c *gin.Context) {
	animeId, err := strconv.Atoi(c.Param("anime"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}
	userId := token.GetIdFromJWT(c)
	val, restErr := a.service.IsFavourite(userId, animeId)
	if restErr != nil {
		c.JSON(restErr.Status(), restErr)
	}
	c.JSON(http.StatusOK, val)
}

func (a *application) GetAllFavouriteAnime(c *gin.Context) {
	userId := token.GetIdFromJWT(c)
	response, errGet := a.service.GetAllFavouriteAnime(userId)
	if errGet != nil {
		c.JSON(errGet.Status(), errGet)
	}
	c.JSON(http.StatusOK, response)
}
