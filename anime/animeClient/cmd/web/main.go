package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v4/pgxpool"
	"google.golang.org/grpc"
	"log"
	"os"
	"saFinal/anime/animeClient/services"
	"saFinal/pb"
	"saFinal/utils/token"
)

type application struct {
	client   pb.AnimeServiceClient
	errorLog *log.Logger
	infoLog  *log.Logger
	router   *gin.Engine
	service  AnimeClientServiceInterface
}

func main() {
	username := flag.String("username", "postgres", "username")
	password := flag.String("password", "postgres", "password")

	host := flag.String("host", "localhost", "host")
	port := flag.String("port", "5432", "port")
	dbname := flag.String("dbname", "anime", "dbname")
	addr := flag.String("addr", ":4015", "HTTP network address")
	secret := flag.String("secret", "NdRgUkXp2s5v8y/B?E(G+KbPeShVmYq3", "Secret key")
	flag.Parse()

	err := os.Setenv("SECRET", *secret)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to set secret key: %v\n", err)
		os.Exit(1)
	}

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	connString := fmt.Sprintf("postgres://%v:%v@%v:%v/%v", *username, *password, *host, *port, *dbname)

	conn, err := pgxpool.Connect(context.Background(), connString)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	router := gin.Default()
	conStr := *host + ":4010"
	connGrpc, err := grpc.Dial(conStr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer connGrpc.Close()
	clientGrpc := pb.NewAnimeServiceClient(connGrpc)
	clientService := services.AnimeClientService{Client: clientGrpc}

	app := &application{
		client:   clientGrpc,
		errorLog: errorLog,
		infoLog:  infoLog,
		router:   router,
		service:  clientService,
	}
	app.router.Use(token.VerifyMiddleware())
	app.routes()

	app.infoLog.Printf("Starting server on %s", *addr)
	err = router.Run(*addr)
	if err != nil {
		app.errorLog.Fatal(err)
	}
}
