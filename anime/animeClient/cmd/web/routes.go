package main

import "saFinal/utils/token"

func (a *application) routes() {
	a.router.POST("/anime", token.IsAdmin(), a.AddAnime)
	a.router.GET("/anime/:id", a.GetAnime)
	a.router.GET("/anime", a.GetAllAnime)
	a.router.GET("/search/:keyword", a.SearchAnime)
	a.router.DELETE("/anime/:id", token.IsAdmin(), a.DeleteAnime)
	a.router.POST("/favourite/:anime", a.AddToFavourite)
	a.router.GET("/favourite/:anime", a.IsFavourite)
	a.router.GET("/favourite", a.GetAllFavouriteAnime)
}
