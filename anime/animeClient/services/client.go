package services

import (
	"context"
	"io"
	anime "saFinal/anime/animeServer/domain"
	"saFinal/foundation/rest_errors"
	"saFinal/pb"
)

type AnimeClientService struct {
	Client pb.AnimeServiceClient
}

func (a AnimeClientService) GetAnime(animeId int) (*anime.Anime, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.GetAnimeByIdRequest{Id: int32(animeId)}
	response, err := a.Client.GetAnimeById(ctx, request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during getting anime from grpc server", err)
	}
	animeObj := &anime.Anime{
		Id:          response.Id,
		Title:       response.Title,
		Description: response.Description,
	}
	return animeObj, nil
}

func (a AnimeClientService) AddAnime(animeObj *anime.Anime) (int32, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.Anime{
		Id:          0,
		Title:       animeObj.Title,
		Description: animeObj.Description,
	}
	response, err := a.Client.AddAnime(ctx, request)
	if err != nil {
		return -1, rest_errors.NewInternalServerError("error during adding anime in grpc server", err)
	}
	return response.Id, nil
}

func (a AnimeClientService) GetAllAnime() ([]*anime.Anime, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.EmptyRequest{}
	stream, err := a.Client.GetAllAnime(ctx, request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during getting all anime from grpc server", err)
	}
	defer stream.CloseSend()
	var animeList []*anime.Anime
LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			return nil, rest_errors.NewInternalServerError("error while receiving anime from grpc server", err)
		}
		animeObj := &anime.Anime{
			Id:          res.Id,
			Title:       res.Title,
			Description: res.Description,
		}
		animeList = append(animeList, animeObj)
	}

	return animeList, nil
}

func (a AnimeClientService) SearchAnime(keyword string) ([]*anime.Anime, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.SearchRequest{SearchString: keyword}
	stream, err := a.Client.SearchAnime(ctx, request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during search anime from grpc server", err)
	}
	defer stream.CloseSend()
	var animeList []*anime.Anime
LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			return nil, rest_errors.NewInternalServerError("error while receiving search anime from grpc server", err)
		}
		animeObj := &anime.Anime{
			Id:          res.Id,
			Title:       res.Title,
			Description: res.Description,
		}
		animeList = append(animeList, animeObj)
	}

	return animeList, nil
}

func (a AnimeClientService) DeleteAnime(animeId int) (int32, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.GetAnimeByIdRequest{Id: int32(animeId)}
	response, err := a.Client.DeleteAnime(ctx, request)
	if err != nil {
		return -1, rest_errors.NewInternalServerError("error during deleting anime in grpc server", err)
	}
	return response.Id, nil
}

func (a AnimeClientService) AddToFavourite(userId int, animeId int) (bool, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.AddToFavouriteRequest{
		User: &pb.User{
			Id:       int64(userId),
			Username: "",
		},
		Anime: &pb.Anime{
			Id:          int32(animeId),
			Title:       "",
			Description: "",
		},
	}
	response, err := a.Client.AddToFavourite(ctx, request)
	if err != nil {
		return false, rest_errors.NewInternalServerError("error during adding anime to favourite in grpc server", err)
	}
	return response.IsAdded, nil
}

func (a AnimeClientService) IsFavourite(userId int, animeId int) (bool, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.IsFavouriteRequest{
		User: &pb.User{
			Id:       int64(userId),
			Username: "",
		},
		Anime: &pb.Anime{
			Id:          int32(animeId),
			Title:       "",
			Description: "",
		},
	}
	response, err := a.Client.IsFavourite(ctx, request)
	if err != nil {
		return false, rest_errors.NewInternalServerError("error during checking anime for favourite in grpc server", err)
	}
	return response.IsFavourite, nil
}

func (a AnimeClientService) GetAllFavouriteAnime(userId int) ([]*anime.Anime, rest_errors.RestErr) {
	ctx := context.Background()
	request := &pb.User{
		Id:       int64(userId),
		Username: "",
	}
	stream, err := a.Client.GetAllFavouriteAnime(ctx, request)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during getting all favourite anime from grpc server", err)
	}
	defer stream.CloseSend()
	var animeList []*anime.Anime
LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break LOOP
			}
			return nil, rest_errors.NewInternalServerError("error while receiving favourite anime from grpc server", err)
		}
		animeObj := &anime.Anime{
			Id:          res.Id,
			Title:       res.Title,
			Description: res.Description,
		}
		animeList = append(animeList, animeObj)
	}

	return animeList, nil
}
