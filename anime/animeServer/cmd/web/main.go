package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	repo "saFinal/anime/animeServer/repository"
	"saFinal/anime/animeServer/services"
	"saFinal/pb"
)

type application struct {
	pb.UnimplementedAnimeServiceServer
	errorLog *log.Logger
	infoLog  *log.Logger
	service  AnimeServiceInterface
}

func main() {
	username := flag.String("username", "postgres", "username")
	password := flag.String("password", "postgres", "password")

	host := flag.String("host", "localhost", "host")
	port := flag.String("port", "5432", "port")
	dbname := flag.String("dbname", "anime", "dbname")
	addr := flag.String("addr", ":4010", "HTTP network address")

	flag.Parse()
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	connString := fmt.Sprintf("postgres://%v:%v@%v:%v/%v", *username, *password, *host, *port, *dbname)

	conn, err := pgxpool.Connect(context.Background(), connString)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close()

	animeRepository := &repo.AnimeRepository{
		Pool:     conn,
		ErrorLog: errorLog,
		InfoLog:  infoLog,
	}

	animeService := &services.AnimeService{Repository: animeRepository}

	app := &application{
		UnimplementedAnimeServiceServer: pb.UnimplementedAnimeServiceServer{},
		errorLog:                        errorLog,
		infoLog:                         infoLog,
		service:                         animeService,
	}
	addressString := *host + *addr
	l, err := net.Listen("tcp", addressString)
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}

	s := grpc.NewServer()
	pb.RegisterAnimeServiceServer(s, app)

	app.infoLog.Printf("Starting server on %s", *addr)
	if err := s.Serve(l); err != nil {
		app.errorLog.Fatalf("failed to serve:%v", err)
	}
}
