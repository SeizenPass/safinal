package main

import (
	"context"
	"saFinal/anime/animeServer/domain"
	"saFinal/foundation/rest_errors"
	"saFinal/pb"
)

type AnimeServiceInterface interface {
	AddAnime(anime *anime.Anime) (int32, error)
	GetAnimeById(animeId int32) (*anime.Anime, error)
	GetAllAnime() ([]*anime.Anime, error)
	SearchAnime(keyword string) ([]*anime.Anime, error)
	DeleteAnime(animeId int32) (int32, rest_errors.RestErr)
	AddToFavourite(userId int32, anime *anime.Anime) (bool, error)
	IsFavourite(userId int32, anime *anime.Anime) (bool, error)
	GetAllFavouriteAnime(userId int32) ([]*anime.Anime, error)
}

func (a *application) AddAnime(ctx context.Context, req *pb.Anime) (*pb.AnimeIdResponse, error) {
	a.infoLog.Printf("AddAnime function was invoked in gRPC server")
	anime := &anime.Anime{}
	anime.Title = req.GetTitle()
	anime.Description = req.GetDescription()
	id, err := a.service.AddAnime(anime)
	if err != nil {
		return nil, err
	}
	res := &pb.AnimeIdResponse{Id: id}
	return res, nil
}

func (a *application) GetAnimeById(ctx context.Context, req *pb.GetAnimeByIdRequest) (*pb.Anime, error) {
	a.infoLog.Printf("GetAnimeById function was invoked in gRPC server")
	id := req.Id
	anime, err := a.service.GetAnimeById(id)
	if err != nil {
		return nil, err
	}
	res := &pb.Anime{
		Id:          anime.Id,
		Title:       anime.Title,
		Description: anime.Description,
	}
	return res, nil
}

func (a *application) GetAllAnime(req *pb.EmptyRequest, stream pb.AnimeService_GetAllAnimeServer) error {
	a.infoLog.Printf("GetAllAnime function was invoked in gRPC server")
	animeList, err := a.service.GetAllAnime()
	if err != nil {
		return err
	}
	for _, anime := range animeList {
		res := &pb.Anime{
			Id:          anime.Id,
			Title:       anime.Title,
			Description: anime.Description,
		}
		if err := stream.Send(res); err != nil {
			a.errorLog.Printf("error while sending GetAllAnime responses", err.Error())
		}
	}
	return nil
}

func (a *application) SearchAnime(req *pb.SearchRequest, stream pb.AnimeService_SearchAnimeServer) error {
	a.infoLog.Printf("SearchAnime function was invoked in gRPC server")
	keyword := req.SearchString
	animeList, err := a.service.SearchAnime(keyword)
	if err != nil {
		return err
	}
	for _, anime := range animeList {
		res := &pb.Anime{
			Id:          anime.Id,
			Title:       anime.Title,
			Description: anime.Description,
		}
		if err := stream.Send(res); err != nil {
			a.errorLog.Printf("error while sending SearchAnime responses", err.Error())
		}
	}
	return nil
}

func (a *application) DeleteAnime(ctx context.Context, req *pb.GetAnimeByIdRequest) (*pb.AnimeIdResponse, error) {
	a.infoLog.Printf("DeleteAnime function was invoked in gRPC server")
	id := req.Id
	deletedId, err := a.service.DeleteAnime(id)
	if err != nil {
		return nil, err
	}
	res := &pb.AnimeIdResponse{Id: deletedId}
	return res, nil
}

func (a *application) AddToFavourite(ctx context.Context, req *pb.AddToFavouriteRequest) (*pb.AddToFavouriteResponse, error) {
	a.infoLog.Printf("AddToFavourite function was invoked in gRPC server")
	userId := req.User.Id
	anime := &anime.Anime{
		Id:          req.Anime.Id,
		Title:       req.Anime.Title,
		Description: req.Anime.Description,
	}
	isAdded, err := a.service.AddToFavourite(int32(userId), anime)
	if err != nil {
		return nil, err
	}
	res := &pb.AddToFavouriteResponse{IsAdded: isAdded}
	return res, nil
}

func (a *application) IsFavourite(ctx context.Context, req *pb.IsFavouriteRequest) (*pb.IsFavouriteResponse, error) {
	a.infoLog.Printf("IsFavourite function was invoked in gRPC server")
	userId := req.User.Id
	anime := &anime.Anime{
		Id:          req.Anime.Id,
		Title:       req.Anime.Title,
		Description: req.Anime.Description,
	}
	isFavourite, err := a.service.IsFavourite(int32(userId), anime)
	if err != nil {
		return nil, err
	}
	res := &pb.IsFavouriteResponse{IsFavourite: isFavourite}
	return res, nil
}

func (a *application) GetAllFavouriteAnime(req *pb.User, stream pb.AnimeService_GetAllFavouriteAnimeServer) error {
	a.infoLog.Printf("GetAllFavouriteAnime function was invoked in gRPC server")
	uid := req.Id
	animeList, err := a.service.GetAllFavouriteAnime(int32(uid))
	if err != nil {
		return err
	}
	for _, anime := range animeList {
		res := &pb.Anime{
			Id:          anime.Id,
			Title:       anime.Title,
			Description: anime.Description,
		}
		if err := stream.Send(res); err != nil {
			a.errorLog.Printf("error while sending GetAllFavouriteAnime responses", err.Error())
		}
	}
	return nil
}
