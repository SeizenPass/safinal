package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	anime "saFinal/anime/animeServer/domain"
	"saFinal/foundation/rest_errors"
)

const (
	queryGetAllAnime            = "SELECT id, anime_title, description FROM anime"
	queryInsertAnime            = "INSERT INTO anime (anime_title, description) VALUES ($1,$2) RETURNING id"
	queryGetAnime               = "SELECT id, anime_title, description FROM anime WHERE id=$1"
	queryUpdateAnime            = "UPDATE anime SET anime_title=$1, description=$2 WHERE id=$3"
	queryDeleteAnime            = "DELETE FROM anime WHERE id=$1"
	queryFindByTitle            = "SELECT id, anime_title, description FROM anime WHERE upper(anime_title) LIKE upper($1)"
	queryInsertFavouriteAnime   = "INSERT INTO favorited_anime (user_id, anime_id) VALUES ($1, $2)"
	queryFindFavouriteAnimeById = "SELECT count(id) FROM favorited_anime WHERE anime_id=$1 AND user_id=$2"
	queryGetAllFavouriteAnime   = "SELECT anime.id, anime_title, description FROM anime RIGHT JOIN favorited_anime fa on anime.id = fa.anime_id WHERE user_id=$1"
)

type AnimeRepository struct {
	Pool     *pgxpool.Pool
	ErrorLog *log.Logger
	InfoLog  *log.Logger
}

func (r *AnimeRepository) AddToFavourite(userId int32, animeId int32) (bool, rest_errors.RestErr) {
	_, err := r.Pool.Exec(context.Background(), queryInsertFavouriteAnime, userId, animeId)
	if err != nil {
		return false, rest_errors.NewInternalServerError("error during adding animeServer to favourite", err)
	}
	return true, nil
}

func (r *AnimeRepository) IsFavourite(userId int32, animeId int32) (bool, rest_errors.RestErr) {
	var numOfFavourite int32
	row := r.Pool.QueryRow(context.Background(), queryFindFavouriteAnimeById, animeId, userId)
	err := row.Scan(&numOfFavourite)
	if err != nil {
		return false, rest_errors.NewInternalServerError("error during checking animeServer for being favourite", err)
	}
	if numOfFavourite == 0 {
		return false, nil
	}
	return true, nil
}

func (r *AnimeRepository) GetAllFavouriteAnime(userId int32) ([]*anime.Anime, rest_errors.RestErr) {
	rows, err := r.Pool.Query(context.Background(), queryGetAllFavouriteAnime, userId)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during favourite animeServer fetch", err)
	}
	defer rows.Close()

	var animeList []*anime.Anime
	for rows.Next() {
		anime := &anime.Anime{}
		err = rows.Scan(&anime.Id, &anime.Title, &anime.Description)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("error during favourite animeServer parse from database", err)
		}
		animeList = append(animeList, anime)
	}
	if err = rows.Err(); err != nil {
		return nil, rest_errors.NewInternalServerError("error with getting all favourite animes", err)
	}
	return animeList, nil
}

func (r *AnimeRepository) Save(anime *anime.Anime) (*anime.Anime, rest_errors.RestErr) {
	var animeId int32
	row := r.Pool.QueryRow(context.Background(), queryInsertAnime, anime.Title, anime.Description)
	err := row.Scan(&animeId)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("internal server error", err)
	}
	anime.Id = animeId
	return anime, nil
}

func (r *AnimeRepository) Delete(id int32) (int32, rest_errors.RestErr) {
	_, err := r.Pool.Exec(context.Background(), queryDeleteAnime, id)
	if err != nil {
		return -1, rest_errors.NewInternalServerError("error during animeServer delete", err)
	}
	return id, nil
}

func (r *AnimeRepository) Get(id int32) (*anime.Anime, rest_errors.RestErr) {
	row := r.Pool.QueryRow(context.Background(), queryGetAnime, id)
	anime := &anime.Anime{}
	err := row.Scan(&anime.Id, &anime.Title, &anime.Description)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, rest_errors.NewNotFoundError("Anime not found")
		} else {
			return nil, rest_errors.NewInternalServerError("error during anime fetching", err)
		}
	}
	return anime, nil
}

func (r *AnimeRepository) Update(anime *anime.Anime) (*anime.Anime, rest_errors.RestErr) {
	_, err := r.Pool.Exec(context.Background(), queryUpdateAnime, anime.Title, anime.Description)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during anime update", err)
	}
	return anime, nil
}

func (r *AnimeRepository) FindByTitle(title string) ([]*anime.Anime, rest_errors.RestErr) {
	title = "%" + title + "%"
	rows, err := r.Pool.Query(context.Background(), queryFindByTitle, title)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during animes fetch", err)
	}
	defer rows.Close()
	var animes []*anime.Anime
	for rows.Next() {
		anime := &anime.Anime{}
		err = rows.Scan(&anime.Id, &anime.Title, &anime.Description)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("error during anime parse from database", err)
		}
		animes = append(animes, anime)
	}
	if err = rows.Err(); err != nil {
		return nil, rest_errors.NewInternalServerError("error with getting all animes", err)
	}
	return animes, nil
}
func (r *AnimeRepository) GetAll() ([]*anime.Anime, rest_errors.RestErr) {
	rows, err := r.Pool.Query(context.Background(), queryGetAllAnime)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during anime fetch", err)
	}
	defer rows.Close()

	var animeList []*anime.Anime
	for rows.Next() {
		anime := &anime.Anime{}
		err = rows.Scan(&anime.Id, &anime.Title, &anime.Description)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("error during anime parse from database", err)
		}
		animeList = append(animeList, anime)
	}
	if err = rows.Err(); err != nil {
		return nil, rest_errors.NewInternalServerError("error with getting all animes", err)
	}
	return animeList, nil
}
