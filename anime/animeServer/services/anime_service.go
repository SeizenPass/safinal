package services

import (
	"saFinal/anime/animeServer/domain"
	"saFinal/foundation/rest_errors"
)

type AnimeService struct {
	Repository Repository
}

type Repository interface {
	Get(id int32) (*anime.Anime, rest_errors.RestErr)
	Save(anime *anime.Anime) (*anime.Anime, rest_errors.RestErr)
	Update(anime *anime.Anime) (*anime.Anime, rest_errors.RestErr)
	Delete(id int32) (int32, rest_errors.RestErr)
	FindByTitle(title string) ([]*anime.Anime, rest_errors.RestErr)
	GetAll() ([]*anime.Anime, rest_errors.RestErr)
	AddToFavourite(userId int32, animeId int32) (bool, rest_errors.RestErr)
	IsFavourite(userId int32, animeId int32) (bool, rest_errors.RestErr)
	GetAllFavouriteAnime(userId int32) ([]*anime.Anime, rest_errors.RestErr)
}

func (a AnimeService) UpdateAnime(anime *anime.Anime) (*anime.Anime, error) {
	current, err := a.Repository.Get(anime.Id)
	if err != nil {
		return nil, err
	}
	if anime.Title == "" || anime.Description == "" {
		return nil, rest_errors.NewBadRequestError("title and description must not be null")
	}
	current.Title = anime.Title
	current.Description = anime.Description
	current, err = a.Repository.Update(current)
	if err != nil {
		return nil, err
	}
	return current, nil
}

func (a AnimeService) AddAnime(anime *anime.Anime) (int32, error) {
	animeSaved, err := a.Repository.Save(anime)
	if err != nil {
		return -1, err
	}
	return animeSaved.Id, nil
}

func (a AnimeService) GetAnimeById(animeId int32) (*anime.Anime, error) {
	anime, err := a.Repository.Get(animeId)
	if err != nil {
		return nil, err
	}
	return anime, nil
}

func (a AnimeService) GetAllAnime() ([]*anime.Anime, error) {
	animeList, err := a.Repository.GetAll()
	if err != nil {
		return nil, err
	}
	return animeList, nil
}

func (a AnimeService) SearchAnime(keyword string) ([]*anime.Anime, error) {
	anime, err := a.Repository.FindByTitle(keyword)
	if err != nil {
		return nil, err
	}
	return anime, nil
}

func (a AnimeService) DeleteAnime(animeId int32) (int32, rest_errors.RestErr) {
	id, err := a.Repository.Delete(animeId)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (a AnimeService) AddToFavourite(userId int32, anime *anime.Anime) (bool, error) {
	return a.Repository.AddToFavourite(userId, anime.Id)
}

func (a AnimeService) IsFavourite(userId int32, anime *anime.Anime) (bool, error) {
	return a.Repository.IsFavourite(userId, anime.Id)
}

func (a AnimeService) GetAllFavouriteAnime(userId int32) ([]*anime.Anime, error) {
	return a.Repository.GetAllFavouriteAnime(userId)
}
