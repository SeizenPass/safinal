package services

import (
	"saFinal/foundation/rest_errors"
	//users "authService/domain"
	//"authService/foundation/rest_errors"
	//"authService/utils/crypto"
	//"authService/utils/date"
	users "saFinal/user/domain"
	"saFinal/utils/crypto"
)

type UsersService struct {
	Repository Repository
}

type Repository interface {
	GetAll() ([]*users.User, rest_errors.RestErr)
	Get(id int64) (*users.User, rest_errors.RestErr)
	Save(user *users.User) (*users.User, rest_errors.RestErr)
	SaveWithRole(user *users.User) (*users.User, rest_errors.RestErr)
	Update(user *users.User) (*users.User, rest_errors.RestErr)
	Delete(id int64) rest_errors.RestErr
	FindByEmailAndPassword(email string, password string) (*users.User, rest_errors.RestErr)
}

func (s *UsersService) GetUser(userId int64) (*users.User, rest_errors.RestErr) {
	user, err := s.Repository.Get(userId)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UsersService) CreateUser(user *users.User, withRole bool) (*users.User, rest_errors.RestErr) {
	if err := user.Validate(); err != nil {
		return nil, err
	}
	user.Password = crypto.GetMd5(user.Password)
	var userSaved *users.User
	var err rest_errors.RestErr
	if withRole {
		userSaved, err = s.Repository.SaveWithRole(user)
	} else {
		userSaved, err = s.Repository.Save(user)
	}
	if err != nil {
		return nil, err
	}
	return userSaved, nil
}

func (s *UsersService) UpdateUser(user *users.User) (*users.User, rest_errors.RestErr) {
	current, err := s.Repository.Get(user.Id)
	if err != nil {
		return nil, err
	}
	if user.Password == "" || user.Username == "" {
		return nil, rest_errors.NewBadRequestError("password and username must not be null")
	}
	current.Username = user.Username
	current.Password = user.Password
	current, err = s.Repository.Update(current)
	if err != nil {
		return nil, err
	}
	return current, nil
}

func (s *UsersService) DeleteUser(userId int64) rest_errors.RestErr {
	return s.Repository.Delete(userId)
}

func (s *UsersService) LoginUser(request *users.LoginRequest) (*users.User, rest_errors.RestErr) {
	usr, err := s.Repository.FindByEmailAndPassword(request.Username, crypto.GetMd5(request.Password))
	if err != nil {
		return nil, err
	}
	return usr, nil
}

func (s *UsersService) GetAllUsers() ([]*users.User, rest_errors.RestErr) {
	return s.Repository.GetAll()
}
