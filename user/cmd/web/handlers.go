package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"saFinal/foundation/rest_errors"
	users "saFinal/user/domain"
	"strconv"
)

type UserServiceInterface interface {
	GetAllUsers() ([]*users.User, rest_errors.RestErr)
	GetUser(int64) (*users.User, rest_errors.RestErr)
	CreateUser(*users.User, bool) (*users.User, rest_errors.RestErr)
	UpdateUser(*users.User) (*users.User, rest_errors.RestErr)
	DeleteUser(int64) rest_errors.RestErr
}

func (a *application) GetAllUsers(c *gin.Context) {
	usersList, err := a.service.GetAllUsers()
	if err != nil {
		c.JSON(err.Status(), err)
		return
	}
	var res users.Users
	res = usersList
	c.JSON(http.StatusOK, res.Marshall())
}

func (a *application) GetUser(c *gin.Context) {
	user := &users.User{}
	i, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid path parameter")
		c.JSON(restErr.Status(), restErr)
		return
	}
	user.Id = int64(i)

	userResponse, errorGet := a.service.GetUser(user.Id)
	if errorGet != nil {
		c.JSON(errorGet.Status(), errorGet)
		return
	}

	c.JSON(http.StatusOK, userResponse.Marshall())
}

func (a *application) CreateUser(c *gin.Context) {
	user := &users.User{}
	if err := c.ShouldBindJSON(user); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}
	result, saveErr := a.service.CreateUser(user, true)

	if saveErr != nil {
		c.JSON(saveErr.Status(), saveErr)
		return
	}
	//c.JSON(http.StatusCreated, result.Marshall(c.GetHeader("X-Public") == "true"))
	c.JSON(http.StatusCreated, result.Marshall())
}

func (a *application) DeleteUser(c *gin.Context) {
	i, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}
	errorGet := a.service.DeleteUser(int64(i))
	if errorGet != nil {
		c.JSON(errorGet.Status(), errorGet)
		return
	}
	c.Writer.WriteHeader(http.StatusNoContent)
}

func (a *application) UpdateUser(c *gin.Context) {
	user := &users.User{}
	if err := c.ShouldBindJSON(user); err != nil {
		restErr := rest_errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status(), restErr)
		return
	}
	result, updateErr := a.service.UpdateUser(user)

	if updateErr != nil {
		c.JSON(updateErr.Status(), updateErr)
		return
	}

	c.JSON(http.StatusOK, result)
}
