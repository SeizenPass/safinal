package main

import "saFinal/utils/token"

func (a *application) routes() {
	a.router.GET("/users", a.GetAllUsers)                        //get all users
	a.router.GET("/users/:id", a.GetUser)                        // get particular user
	a.router.POST("/users", token.IsAdmin(), a.CreateUser)       //create user
	a.router.PUT("/users", token.IsAdmin(), a.UpdateUser)        //modify user
	a.router.DELETE("/users/:id", token.IsAdmin(), a.DeleteUser) //delete user by id
}
