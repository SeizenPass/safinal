package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"saFinal/foundation/rest_errors"
	"saFinal/user/domain"
	"saFinal/utils/crypto"
	_ "strings"
)

const (
	queryGetAllUsers            = "SELECT u.id, u.username, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id"
	queryInsertUser             = "INSERT INTO users (username, password) VALUES ($1,$2) RETURNING id"
	queryInsertUserWithRole     = "INSERT INTO users (username, password, role_id) VALUES ($1,$2, $3) RETURNING id"
	queryGetUser                = "SELECT u.id, u.username, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id WHERE id=$1"
	queryUpdateUser             = "UPDATE users SET username=$1, password=$2, role_id=$3 WHERE id=$4"
	queryDeleteUser             = "DELETE FROM users WHERE id=$1"
	queryFindByEmailAndPassword = "SELECT u.id, u.username, r.role_name FROM users u JOIN roles r on r.role_id = u.role_id WHERE username=$1 AND password=$2"
	queryGetRoleIdByRoleName    = "SELECT role_id FROM roles WHERE role_name=$1"
)

type UserRepository struct {
	Pool     *pgxpool.Pool
	ErrorLog *log.Logger
	InfoLog  *log.Logger
}

func (r *UserRepository) GetAll() ([]*users.User, rest_errors.RestErr) {
	rows, err := r.Pool.Query(context.Background(), queryGetAllUsers)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during users fetch", err)
	}
	defer rows.Close()

	var userList []*users.User
	for rows.Next() {
		user := &users.User{}
		err = rows.Scan(&user.Id, &user.Username, &user.Role)
		if err != nil {
			return nil, rest_errors.NewInternalServerError("error during user parse from database", err)
		}
		userList = append(userList, user)
	}
	if err = rows.Err(); err != nil {
		return nil, rest_errors.NewInternalServerError("error with getting all users", err)
	}
	return userList, nil
}

func (r *UserRepository) Get(id int64) (*users.User, rest_errors.RestErr) {
	row := r.Pool.QueryRow(context.Background(), queryGetUser, id)
	user := &users.User{}
	err := row.Scan(&user.Id, &user.Username, &user.Role)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, rest_errors.NewNotFoundError("User not found")
		} else {
			return nil, rest_errors.NewInternalServerError("error during user fetching", err)
		}
	}
	return user, nil
}

func (r *UserRepository) Save(user *users.User) (*users.User, rest_errors.RestErr) {
	var userId int64
	row := r.Pool.QueryRow(context.Background(), queryInsertUser, user.Username, user.Password)
	err := row.Scan(&userId)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("internal server error", err)
	}
	user.Id = userId
	user.Role = "USER"
	return user, nil
}

func (r *UserRepository) SaveWithRole(user *users.User) (*users.User, rest_errors.RestErr) {
	var userId int64
	roleId, errRole := r.GetRoleIdByName(user.Role)
	if errRole != nil {
		return nil, errRole
	}
	row := r.Pool.QueryRow(context.Background(), queryInsertUserWithRole, user.Username, user.Password, roleId)
	err := row.Scan(&userId)
	if err != nil {
		return nil, rest_errors.NewInternalServerError("internal server error", err)
	}
	user.Id = userId
	return user, nil
}

func (r *UserRepository) GetRoleIdByName(roleName string) (int64, rest_errors.RestErr) {
	var roleId int64
	row := r.Pool.QueryRow(context.Background(), queryGetRoleIdByRoleName, roleName)
	err := row.Scan(&roleId)
	if err != nil {
		return -1, rest_errors.NewInternalServerError("internal server error", err)
	}
	return roleId, nil
}

func (r *UserRepository) Update(user *users.User) (*users.User, rest_errors.RestErr) {
	roleId, roleErr := r.GetRoleIdByName(user.Role)
	if roleErr != nil {
		return nil, roleErr
	}
	_, err := r.Pool.Exec(context.Background(), queryUpdateUser, user.Username, crypto.GetMd5(user.Password), roleId, int(user.Id))
	if err != nil {
		return nil, rest_errors.NewInternalServerError("error during user update", err)
	}
	return user, nil
}

func (r *UserRepository) Delete(id int64) rest_errors.RestErr {
	_, err := r.Pool.Exec(context.Background(), queryDeleteUser, id)
	if err != nil {
		return rest_errors.NewInternalServerError("error during user delete", err)
	}
	return nil
}

func (r *UserRepository) FindByEmailAndPassword(username string, password string) (*users.User, rest_errors.RestErr) {
	row := r.Pool.QueryRow(context.Background(), queryFindByEmailAndPassword, username, password)
	user := &users.User{}
	err := row.Scan(&user.Id, &user.Username, &user.Role)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, rest_errors.NewNotFoundError("user not found or password is not correct")
		} else {
			return nil, rest_errors.NewInternalServerError("internal server error", err)
		}
	}
	return user, nil
}
