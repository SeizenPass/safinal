package users

type PublicUser struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
	Role     string `json:"role"`
}

type PrivateUser struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
	Role     string `json:"role"`
	Password string `json:"password"`
}

func (users Users) Marshall() []interface{} {
	result := make([]interface{}, len(users))
	for index, user := range users {
		result[index] = user.Marshall()
	}
	return result
}

func (user *User) Marshall() interface{} {
	return PublicUser{
		Id:       user.Id,
		Username: user.Username,
		Role:     user.Role,
	}

	/*userJson, _ := json.Marshal(user)
	var privateUser PrivateUser
	json.Unmarshal(userJson, &privateUser)
	return privateUser*/
}
