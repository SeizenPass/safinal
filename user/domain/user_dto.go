package users

import (
	"saFinal/foundation/rest_errors"
	"strings"
)

type User struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type Users []*User

func (user *User) Validate() rest_errors.RestErr {
	user.Password = strings.TrimSpace(user.Password)
	user.Username = strings.TrimSpace(user.Username)
	if user.Password == "" {
		return rest_errors.NewBadRequestError("invalid password")
	}
	if user.Username == "" {
		return rest_errors.NewBadRequestError("invalid username")
	}
	return nil
}
