package token

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"os"
	"saFinal/foundation/rest_errors"
	"strings"
)

type authHeader struct {
	IDToken string `header:"Authorization"`
}

func GetIdFromJWT(c *gin.Context) int {
	tokenObj := c.MustGet("token").(*jwt.Token)
	claims := tokenObj.Claims.(jwt.MapClaims)
	userId := claims["userId"].(float64)
	return int(userId)
}

func VerifyMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHead := c.Request.Header.Get("Authorization")

		idTokenHeader := strings.Split(authHead, "Bearer ")

		if len(idTokenHeader) < 2 {
			restErr := rest_errors.NewBadRequestError("invalid authorization header")
			c.JSON(restErr.Status(), restErr)
			c.Abort()
			return
		}
		authToken := idTokenHeader[1]
		tokenObj, parseErr := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("error during signing")
			}
			return []byte(os.Getenv("SECRET")), nil
		})

		if parseErr != nil {
			restErr := rest_errors.NewInternalServerError("error during authorization header parsing", parseErr)
			c.JSON(restErr.Status(), restErr)
			c.Abort()
			return
		}

		if tokenObj.Valid {
			//TODO verify token time
			c.Set("token", tokenObj)
			c.Next()
		} else {
			restErr := rest_errors.NewUnauthorizedError("invalid token")
			c.JSON(restErr.Status(), restErr)
			c.Abort()
			return
		}
	}
}

func IsAdmin() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenObj := c.MustGet("token").(*jwt.Token)
		claims := tokenObj.Claims.(jwt.MapClaims)
		if claims["role"] == "ADMIN" {
			c.Next()
		} else {
			restErr := rest_errors.NewUnauthorizedError("no access")
			c.JSON(restErr.Status(), restErr)
		}
		c.Abort()
	}
}
